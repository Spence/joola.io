<img src="http://i.imgur.com/Kx6l8s3.png" alt="joola.io logo" title="joola.io" align="right" />

Welcome to **joola.io** - the open-source data analytics framework.

This wiki is the main source of documentation for **developers** working with (or contributing to) the joola.io project.
If this is your first time hearing about joola.io, we recommend starting with the [joola.io website][website].

>
**NOTE**: The project is undergoing a refactoring process, this means that some of the code and wiki content may change over the course of the next few weeks. We recommend you keep updated via one of our [channels](talk-to-us).


###Quick navigation

| **[Tech Docs] [techdocs]**     | **[Setup Guide] [setup]**     | **[API Docs] [api-docs]**           | **[Contributing] [contributing]**           | **[About] [about]**     |
|-------------------------------------|-------------------------------|-----------------------------------|---------------------------------------------|-------------------------------------|
| [![i1] [techdocs-image]] [techdocs] | [![i2] [setup-image]] [setup] | [![i3] [api-docs-image]] [api-docs] | [![i4] [contributing-image]] [contributing] | [![i5] [about-image]] [about] |


[website]: http://joola.io

[architecture-doc]: https://github.com/joola/joola.io/wiki/Technical-architecture
[talk-to-us]: https://github.com/joola/joola.io/wiki/Talk-to-us

[about-image]: https://raw.github.com/joola/joola.io/develop/docs/images/about.png
[techdocs-image]: https://raw.github.com/joola/joola.io/develop/docs/images/techdocs.png
[setup-image]: https://raw.github.com/joola/joola.io/develop/docs/images/setup.png
[api-docs-image]: https://raw.github.com/joola/joola.io/develop/docs/images/roadmap.png
[contributing-image]: https://raw.github.com/joola/joola.io/develop/docs/images/contributing.png
[issues-image]: https://raw.github.com/joola/joola.io/develop/docs/images/issues.png

[about]: https://github.com/joola/joola.io/wiki/joola.io-overview
[techdocs]: https://github.com/joola/joola.io/wiki/Technical-documentation
[setup]: https://github.com/joola/joola.io/wiki/Setting-up-joola.io
[api-docs]: http://docs.joolaio.apiary.io/
[contributing]: https://github.com/joola/joola.io/wiki/Contributing
[issues]: https://github.com/joola/joola.io/issues

