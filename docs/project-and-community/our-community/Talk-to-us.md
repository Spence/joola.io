<a name="top" />

[HOME](Home) > [PROJECT & COMMUNITY](project-and-community) > **Talk to us**


We want to make it super-easy for joola.io users and contributors to talk to us and connect with each other, to share ideas,
solve problems and help make joola.io awesome.
Here are the main channels we're running currently, we'd love to hear from you on one of them:

## Gitter
[![Gitter chat](https://badges.gitter.im/joola/joola.io.png)](https://gitter.im/joola/joola.io)

We're big fans of Gitter here at joola. We have a `#joola/joola.io` channel - stop by and say hi. Soon, this channel will be linked to our GitHub - so you can see new issues, merged pull requests and so on as they happen.

## Twitter

[@joolaio] [joolaio-twitter]

Follow and chat to us on Twitter.

## GitHub

[joola.io issues] [issues]

If you spot a bug, then please raise an issue in our main GitHub project (`joola/joola.io`); likewise if you have developed a cool new feature or improvement in your joola.io fork, then send us a pull request!

If you want to brainstorm a potential new feature, then the joola.io Google Group (see above) is probably a better place to start.

## Email

[community@joo.la] [community-email]

If you want to talk directly to us (e.g. about a commercially sensitive issue), email is the easiest way.

[google-group]: https://groups.google.com/
[joolaio-twitter]: https://twitter.com/joolaio
[new-issue]: https://github.com/joola/joola.io/issues/new
[issues]: https://github.com/joola/joola.io/issues?direction=desc&sort=created&state=open
[community-email]: mailto:community@joo.la
[freenode-webchat]: http://webchat.freenode.net/