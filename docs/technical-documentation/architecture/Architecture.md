[HOME](Home) > [TECHNICAL DOCUMENTATION](technical-documentation) > **ARCHITECTURE**

- [Core](The-Core-Subsystem) - Internal processes
- [Common](The-Common-Subsystem) - Shared modules and code
- [Dispatch](The-Dispatch-Subsystem) - The grid messaging system
- [Query](The-Query-Subsystem) - Manages the aspects of querying the system
- [Beacon](The-Beacon-Subsystem) - Handles the framework's internal cache
- [Authentication](The-Authentication-Subsystem) - All authentication aspects of the framework
- [Web Server](The-Webserver-Subsystem) - Serves web content to end users
- [SDK](The-SDK-Subsystem) - Used to communicate with joola.io framework and manage it