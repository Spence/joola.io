[HOME](Home) > **SDK**

joola.io arrives with a full Client [Software Development Kit (SDK)](http://github.com/joola/joola.io.sdk) designed to allow developers to easily connect, manage, push and consume data from joola.io

The following sections cover the different aspects of the SDK and its usage. 

- [Getting and using the SDK](using-the-sdk)
- [Security and authentication](security-and-authentication)
- [Pushing data](pushing-data)
- [Query, analytics and visualization](analytics-and-visualization)
- [Collections and meta data](collections)
- [Workspaces, users and roles](user-management)
- [System health and stats](system-health)
- [**Complete API documentation**](sdk-api-documentation)

Also, make sure you check our [**examples**](examples) and [**workshops**](workshops).
