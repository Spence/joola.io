[**HOME**](Home) > **TECHNICAL DOCUMENTATION**

The technical documentation review the joola.io architecture, know-how and details breakdown of internal processes.

## Getting Started
- [Setup joola.io](setting-up-joola.io)
- [Push Data](pushing-data)
- [Query, Analyze and Visualize](analytics-and-visualization)

#### Useful Resources
- [[Basic Concepts]]
- [API Documentation](api-documentation)
- [[Using the SDK]]
- [[Examples]]
- [[Workshops]]
- [[Labs]]

## Architecture
We've tried to keep things simple and divided the framework into the listed logical entities, each is aimed at serving a different aspect of the framework.

- [Overview](architecture) - Internal processes
- [Core](The-Core-Subsystem) - Internal processes
- [Common](The-Common-Subsystem) - Shared modules and code
- [Dispatch](The-Dispatch-Subsystem) - The grid messaging system
- [Query](The-Query-Subsystem) - Manages the aspects of querying the system
- [Beacon](The-Beacon-Subsystem) - Handles the framework's internal cache
- [Authentication](The-Authentication-Subsystem) - All authentication aspects of the framework
- [Web Server](The-Webserver-Subsystem) - Serves web content to end users
- [SDK](The-SDK-Subsystem) - Used to communicate with joola.io framework and manage it

## The Development Process
We aim to create the world's best mass-scale data analytics framework and for that, we need to have a solid and robust development process.
This section describes the process and protocols we use throughout the development of the framework.

- [The development process](the-development-process)
- [Testing](code-testing)
- [Code Style](code-style-guidelines)
- [Versioning](versioning)
- [Release and publish](software-release-process)
- [[Contributing]]
- [Roadmap](product-roadmap)

## Code documentation
This wiki also includes [REST API documentation](api-documentation).
