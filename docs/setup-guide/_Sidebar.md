[**HOME**](Home) > **SETUP GUIDE**

**Setup joola.io**

- [Step 1: Install joola.io] (install-joola.io)
- [Step 2: Configuration] (Configuration)
- [Step 3: Collections] (setting-up-collections)
- [Step 4: Sending data] (setting-up-first-time-run)
- [Step 5: Visualizations] (getting-started-advanced-configuration)

**Useful resources**

- [[FAQ]]
- [[Troubleshooting]]  
- [[Contributing]]
